const input = document.querySelectorAll(".input-pass");
const eye = document.querySelectorAll(".icon-password");
const btn = document.querySelector(".btn");
let password = document.getElementById("first-password");
let checkPass = document.getElementById("second-password");

eye.forEach(element => {
  element.addEventListener("click", () => {
    const inputType = element.previousElementSibling.getAttribute("type");
    element.classList.toggle("fa-eye-slash");
    if (inputType === "password") {
      element.previousElementSibling.setAttribute("type", "text");
    } else {
      element.previousElementSibling.setAttribute("type", "password");
    }
  });
});

btn.addEventListener("click", () => {
  if (password.value === checkPass.value && password.value !== "" && checkPass !== "") {
    alert("You are welcome!");
  } else {
    let p = document.createElement("p");
    p.innerHTML = "Потрібно ввести однакові значення";
    p.style.color = "red";
    let tag = document.querySelector("section");
    tag.before(p);
  }
});
